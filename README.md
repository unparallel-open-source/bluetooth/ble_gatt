This repository provides all the source code related to the BLE-GATT module, developed in the context of OCARIoT Project.
This BLE-GATT module collects data from IoT enabled devices that provide data through the BLE-GATT profile.


package rest;

import org.eclipse.jetty.client.api.Request;

public interface RestInterface {
	/* 
	 * Class to describe all APIs for BLE-Manager
	 * */
	
	
	// Management specific
	
	/**
	 * Turn gateway on
	 * GET /turnon
	 */
	public final String _path_turnon = "/turnon";

	// Medium access specific
	
	/**
	 *  Used to start discovering the devices in the air medium
	 *  GET /discover
	 *  Response: List of devices in medium
	 */
	public final String _path_discover = "/discover";
	
	// Band specific 
	/**
	 * Connect to a specific band
	 * POST device/band id  
	 */
	public final String _path_connect = "/connect";
	
	/**
	 * GET /bandservices
	 * { @parameters: id - device band id }
	 */
	public final String _path_bandservices = "/bandservices";
	
	// Band specific - service values
	/**
	 * Subscribe to specific band service 
	 * PUT /subscription
	 */
	public final String _path_subscription = "/subscription";
	
	/**
	 * Characteristic subscription
	 * GET
	 */
	public final String _path_subscribe = "/subscribe";
	
	/**
	 * GET values by id, and/or since some timestamp
	 * Response: Values always separated by id
	 */
	public final String _path_values = "/values";
	
	/**
	 * Obtain last N values 
	 * GET 
	 */
	public final String _path_getLast = "/getLast";
	
	// Gateway specific
	
	/**
	 * No parameters (Get list of devices/gateways in the area.
	 * GET /gateways
	 */
	public final String _path_getGateways = "/gateways";
	
	/**
	 * connect band to device/gateway
	 * POST /connGateway
	 */
	public final String _path_connectgateway = "/connGateway";
	

	// All Parameters
	public final String _param_id = "id";
	public final String _param_deviceId = "did";
	public final String _param_read = "read";
	public final String _param_since = "since";
	

}

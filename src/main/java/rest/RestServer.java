package rest;

import javax.xml.ws.Response;

import org.apache.log4j.Logger;
import org.apache.log4j.chainsaw.Main;
import org.json.JSONObject;

import ble.BleManager;
import static spark.Spark.*;

import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;

import storage.DbOperations;
import utils.NetworkInfo;

public class RestServer implements RestInterface {
	
	private final Logger _logger;
	private DbOperations _db;
	private NetworkInfo _netInfo;
	
	public RestServer(int portToListen) {
		_netInfo = new NetworkInfo();
		_logger = Logger.getLogger(RestServer.class);
		_db = BleManager.getDb();
		
		if(portToListen != -1) { port(portToListen); }
		setServerRoutes();
		_logger.info("Server waiting requests at <"+_netInfo.getWiFiAddress()+">");
	}
	
	private void setServerRoutes() {
		setGetRoutes();
		setPostRoutes();
		setPutRoutes();
	}
	
	/**
	 * 
	 */
	private void setGetRoutes() {
		get(_path_turnon, (req, res)-> {
			
			
			Main.main(null);
			
			res.status(200);
			return "";
		});
		
		get(_path_values, (req, res) -> {
			String timestamp = req.queryParams(_param_since);
			String bandId = req.queryParams(_param_id);
			String devId = req.queryParams(_param_deviceId);
			String dataToSend = "";
			
			_logger.info("received "+_path_values+" request from "+req.ip());
			
			if(req.queryParams().size() == 0) {
				_logger.info("Listing all data");
				dataToSend = _db.getAllDataInJson();
//			} else if((bandId != null) && (timestamp != null) && (devId != null)) {
//				_logger.info(message);
			} else if((bandId != null) && (timestamp != null)) {
				_logger.info("Listing id & since: " + bandId + " & " + timestamp);
				dataToSend = _db.getDataByTimestampAndBandIdInJson(bandId, timestamp);
			} else if(timestamp != null) {
				_logger.info("Listing since: " + timestamp);
				dataToSend = _db.getDataByTimestampInJson(timestamp);
			} else if(bandId != null) {
				_logger.info("Listing id: " + bandId);
				dataToSend = _db.getDataByBandIdInJson(bandId);
			} else {
				res.status(400);
				return sendRequestError("Invalid GET Request!", "Valid GET requests are \"/values\", \"/values?since=\", \"/values?id=\" and \"/values?id=&since=\"");
			}
			
			res.status(200);
			return dataToSend;
		});

		
		get(_path_subscribe, (req, res)-> {res.status(200); return "";});
		
		get(_path_bandservices, (req, res)-> {
//			String deviceId = req.queryParams(_param_id);

			String services = _db.getServicesInJson();
			
			_logger.info("Received "+_path_bandservices+" request from "+req.ip());
			_logger.info("Service list request received: "+services);
			
			res.status(200); 
			return services;
			
		});
		get(_path_discover, (req, res)-> {
		
			_logger.info("received "+_path_discover+" request from "+req.ip());
			
			JSONObject answer = new JSONObject();
			
//			String bandId = req.queryParams(_param_id);
			
			String bandId = (req.queryParams(_param_id) != null)? req.queryParams(_param_id):"";
			
			answer.put("Band List",_db.getBandsInJson(bandId));
			answer.put("Answer", "Get Device List OK UI");
			res.status(200);
			return answer;
		});
		
	}
	
	private void setPostRoutes() {
		
		post(_path_connect, (req, res)->{

			return "";
		});
		
		
		
		
		
		post(_path_values, (req, res) -> {
			
			
			
			return "";
		});
		
		post(_path_discover, (req, res) -> {
			
			_logger.info("received "+_path_discover+" request from "+req.ip());
			
			JSONObject answer = new JSONObject();
			answer.put("Answer", "Discover OK UI");
			res.status(200);
			return answer;
		});

		
		
		
	}
	

	
	private void setPutRoutes() {
		put(_path_subscription, (req, res) -> {
			String deviceId = req.queryParams(_param_id);
			String readParam = req.queryParams(_param_read);
			
			_logger.info("received "+_path_subscription+" request from "+req.ip());
			
			if(readParam != null) {
				if(!readParam.equals("true") && !readParam.equals("false")) {
					res.status(400);
					return sendRequestError("Invalid Read Option!", "");
				}
				
				boolean read = Boolean.parseBoolean(readParam);
				
				if(deviceId != null) {
					if(_db.updateDeviceDocument(deviceId, read)) {
						res.status(201);
						return ("Updated device " + deviceId + " readability to " + read + "\n");
					}
				} else {
					_db.updateAllDeviceDocuments(read);
					res.status(201);
					return ("Updated all devices' readability to " + read + "\n");
				}
			}
			
/*			if((bandId != null) && (readParam != null)) {
				if(readParam != "true" && readParam != "false") {
					response.status(400);
					return sendRequestError("Invalid Read Option!", "");
				}
				
				boolean read = Boolean.parseBoolean(readParam);
				if(_db.updateBandDocument(bandId, read)) {
					response.status(201);
					return ("Updated band " + bandId);
				}
			}*/
			
			res.status(400);
			return sendRequestError("Invalid PUT Request!", "Valid PUT requests are \"/subscriptions?id=&read=\" and \"/subscriptions?read=\"");
		});
	}
	
	private String sendRequestError(String error, String validRequest) {
		_logger.info(error);
		return error + "\n" + validRequest + "\n";
	}
	
	/*	private void setGetRoutes() {
	Spark.get("/values", (request, response) -> {
		switch(request.queryParams().size()) {
			case 0: {
				_logger.info("Listing data");
				return _db.getAllDataInJson();
			}
			case 1: {
				if(request.queryParams("since") != null) {
					_logger.info("Listing since: " + request.queryParams("since"));
					return _db.getDataByTimestampInJson(request.queryParams("since"));
				} else if(request.queryParams("id") != null) {
					_logger.info("Listing id: " + request.queryParams("id"));
					return _db.getDataByBandIdInJson(request.queryParams("id"));
				}
				break;
			}
			case 2: {
				if((request.queryParams("since") != null) && (request.queryParams("id") != null)) {
					_logger.info("Listing since & id: " + request.queryParams("since") + " & " + request.queryParams("id"));
					return _db.getDataByTimestampAndBandIdInJson(request.queryParams("id"), request.queryParams("since"));
				}
				break;
			}
			default: break;
		}
		
		response.status(400);
		return sendRequestError("Invalid GET Request!", "Valid GET requests are \"/values\", \"/values?since=\", \"/values?id=\" and \"/values?id=&since=\"");
		});
	}*/
	
	/*	private void setPostRoutes() {
		Spark.post("/subscriptions", (request, response) -> {
			if((request.queryParams("id") != null) && (request.queryParams("url") != null)) {
				String bandId = request.queryParams("id");
				String dataUrl = request.queryParams("url");
				Device band = new Device(bandId, dataUrl, false);
				
				if(_db.insertBandDocument(band)) {
					response.status(201);
					return ("Added band " + band.getDeviceId());
				} else {
					return "Band already exists";
				}
			}
			
			response.status(400);
			return sendRequestError("Invalid POST Request!", "Valid POST request is \"/subscriptions?id=&url=\"");
		});
	}*/
}

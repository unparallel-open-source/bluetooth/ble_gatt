package utils;

import org.sputnikdev.bluetooth.URL;
import org.sputnikdev.bluetooth.manager.DiscoveredDevice;
import org.sputnikdev.bluetooth.manager.GattService;

public interface CallbackListener {

	public void updateDiscoveredDevices(DiscoveredDevice discoveredDevice);
	
	public void updateResolvedServices(String deviceUrl, GattService gattService);
	
	public void updateCharacteristics(GattService gattService, URL characteristicUrl);
	
	public void updateRawData(byte[] rawData, String fieldName, String characteristicUrl);
	
	public void updateParsedData(String parsedData, String fieldUrl);
	
	public void setDeviceListenerScheduler(DiscoveredDevice discoveredDevice);
}

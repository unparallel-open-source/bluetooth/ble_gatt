package utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.HashMap;

import org.apache.log4j.Logger;

import rest.RestServer;

public class NetworkInfo {
    
	HashMap<String, String> ipAddressesMap;
	private final Logger _logger;
	
	public NetworkInfo() {
		_logger = Logger.getLogger(NetworkInfo.class);
		ipAddressesMap = new HashMap();
		
		try {
			this.acquireIpV4();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void acquireIpV4() throws Exception {
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();

        while (interfaces.hasMoreElements()) {
            NetworkInterface networkInterface = interfaces.nextElement();
            // drop inactive
            if (!networkInterface.isUp())
                continue;

            
            Enumeration<InetAddress> addresses = networkInterface.getInetAddresses();
            while(addresses.hasMoreElements()) {
                InetAddress addr = addresses.nextElement();

                if (isIpV4(addr.getHostAddress())) {
                	ipAddressesMap.put(networkInterface.getDisplayName(), addr.getHostAddress());
                }
                
                
            }
        }
    }
	
	public String getWiFiAddress() {
		_logger.info("Acquiring wifi ip...");
		return (ipAddressesMap.get("wlan0"));
	}
	
	public String getNetAddress() {
		// TODO: Not implemented yet. Will provide ethernet ipv4 address
		return "";
	}
	
	public boolean isWifi(String name) {
		return (name.contains("wlan"));
	}
	
	public boolean isIpV6(String ip) {
		return (ip.contains(":"));
	}
	
	public boolean isIpV4(String ip) {
		return ((ip.contains(".") && (countDots(ip) == 3)));
	}
	
	public int countDots(String name) {
		int dotcount =0;
		while (name.contains(".")) {
			int idx = name.indexOf(".");
			name = name.substring(idx);
			dotcount++;
		}
		return dotcount;
	}
}

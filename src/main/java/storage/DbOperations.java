package storage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.dizitart.no2.Cursor;
import org.dizitart.no2.Document;
import org.dizitart.no2.Filter;
import org.dizitart.no2.FindOptions;
import org.dizitart.no2.IndexOptions;
import org.dizitart.no2.IndexType;
import org.dizitart.no2.Nitrite;
import org.dizitart.no2.NitriteCollection;
import org.dizitart.no2.PersistentCollection;
import org.dizitart.no2.SortOrder;
import org.dizitart.no2.filters.Filters;
import org.dizitart.no2.tool.ExportOptions;
import org.dizitart.no2.tool.Exporter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.sputnikdev.bluetooth.gattparser.spec.Characteristic;
import org.sputnikdev.bluetooth.manager.DiscoveredDevice;
import org.sputnikdev.bluetooth.manager.GattService;

import ble.Band;

public class DbOperations {
	
	/* ------------------------------ FILEPATH CONSTANTS ------------------------------ */
	private static final String FILEPATH = "/usr/project/ble-manager-jar/";
//	private static final String FILEPATH = "/Users/luis.paiva/git/ble_gatt/test_runs/";
//	private static final String DB_FILEPATH = FILEPATH + "test.db";
//	private static final String DB_FILEPATH = FILEPATH + "data.db";
	private static final String DB_FILEPATH = FILEPATH + "ocariot.db";
	private static final String DB_JSON = FILEPATH + "db.json";
	private static final String BANDS_JSON = FILEPATH + "bands.json";
	private static final String DEVICES_JSON = FILEPATH + "devices.json";
	private static final String SERVICES_JSON = FILEPATH + "services.json";
	private static final String CHARACTERISTICS_JSON = FILEPATH + "characteristics.json";
	private static final String FIELDS_JSON = FILEPATH + "fields.json";
	private static final String DATA_JSON = FILEPATH + "data.json";
	private static final String ERROR_JSON = FILEPATH + "error.json"; // This should never happen
	
	/* ----------------------------- COLLECTION CONSTANTS ----------------------------- */
	private static final String BANDS_COLLECTION = "bands";
	private static final String DEVICES_COLLECTION = "devices";
	private static final String SERVICES_COLLECTION = "services";
	private static final String CHARACTERISTICS_COLLECTION = "characteristics";
	private static final String FIELDS_COLLECTION = "fields";
	private static final String DATA_COLLECTION = "data";
	
	/* ---------------------------------- VARIABLES ----------------------------------- */
	private final Logger _logger;
	private Nitrite _db;
	
	/* --------------------------------- CONSTRUCTOR ---------------------------------- */
	public DbOperations() {
		_logger = Logger.getLogger(DbOperations.class);
		_db = Nitrite.builder().compressed().filePath(DB_FILEPATH).openOrCreate();
		_logger.info("Database used: <"+DB_FILEPATH+">");
		createAllCollections();
		setIndexesToAllCollections();
	}
	
	/* --------------------------- SETUP COLLECTION METHODS --------------------------- */
	private void createAllCollections() {
		_db.getCollection(BANDS_COLLECTION);
		/*_db.getCollection(DEVICES_COLLECTION);
		_db.getCollection(SERVICES_COLLECTION);
		_db.getCollection(CHARACTERISTICS_COLLECTION);
		_db.getCollection(FIELDS_COLLECTION);*/
		_db.getCollection(DATA_COLLECTION);
	}
	
	private void setIndexesToAllCollections() {
		setIndexToCollection("bandId", BANDS_COLLECTION, IndexType.Unique);
		/*setIndexToCollection("deviceUrl", DEVICES_COLLECTION, IndexType.Unique);
		setIndexToCollection("serviceUrl", SERVICES_COLLECTION, IndexType.Unique);
		setIndexToCollection("characteristicUrl", CHARACTERISTICS_COLLECTION, IndexType.Unique);
		setIndexToCollection("fieldUrl", FIELDS_COLLECTION, IndexType.Unique);*/
		setIndexToCollection("dataUrl", DATA_COLLECTION, IndexType.NonUnique);
	}
	
	private void setIndexToCollection(String fieldToIndex, String collectionName, IndexType indexType) {
		if((fieldToIndex != null) && (collectionName != null)) {
			NitriteCollection collection = _db.getCollection(collectionName);
			
			if(!collection.hasIndex(fieldToIndex)) {
				collection.createIndex(fieldToIndex, IndexOptions.indexOptions(indexType));
			}
		}
	}
	
	/* ----------------------------- EXPORT DATA METHODS ------------------------------ */
	public void exportDbToJson() {
		Exporter dbExporter = Exporter.of(_db);
		dbExporter.exportTo(DB_JSON);
		
		_logger.info("DB was exported successfully");
	}
	
	public void exportEachCollectionIndividuallyToJson() {
		exportCollectionToJson(DbOperations.BANDS_COLLECTION);
		/*exportCollectionToJson(DbOperations.DEVICES_COLLECTION);
    	exportCollectionToJson(DbOperations.SERVICES_COLLECTION);
    	exportCollectionToJson(DbOperations.CHARACTERISTICS_COLLECTION);
    	exportCollectionToJson(DbOperations.FIELDS_COLLECTION);*/
    	exportCollectionToJson(DbOperations.DATA_COLLECTION);
	}
	
	private void exportCollectionToJson(String collectionName) {
		PersistentCollection<?> collection = _db.getCollection(collectionName);
		List<PersistentCollection<?>> collections = new ArrayList<PersistentCollection<?>>();
		collections.add(collection);
		
		String jsonFilepath = getFilepathToExportToJson(collectionName);
		ExportOptions collectionExportOptions = new ExportOptions();
		collectionExportOptions.setCollections(collections);
		
		Exporter.of(_db).withOptions(collectionExportOptions).exportTo(jsonFilepath);
		
		_logger.info("Collection " + collectionName + " was exported successfully");
	}
	
	private String getFilepathToExportToJson(String collectionName) {
		switch(collectionName) {
			case BANDS_COLLECTION: return BANDS_JSON;
			/*case DEVICES_COLLECTION: return DEVICES_JSON;
			case SERVICES_COLLECTION: return SERVICES_JSON;
			case CHARACTERISTICS_COLLECTION: return CHARACTERISTICS_JSON;
			case FIELDS_COLLECTION: return FIELDS_JSON;*/
			case DATA_COLLECTION: return DATA_JSON;
			default: return ERROR_JSON;
		}
	}
	   
	/* ---------------------------------- GET METHODS --------------------------------- */
	public ArrayList<Band> getBands() {
		Band band;
		String bandId, dataUrl;
		ArrayList<Band> bands = new ArrayList<Band>();
		NitriteCollection bandsCollection = _db.getCollection(BANDS_COLLECTION);
		Cursor bandCursor = bandsCollection.find();
		
		for(Document bandDocument : bandCursor) {
			bandId = (String)bandDocument.get("bandId");
			dataUrl = (String)bandDocument.get("dataUrl");
			band = new Band(bandId, dataUrl);
			bands.add(band);
		}
		
		return bands;
	}
	
	public String getBandsInJson(String bandIdToFilter) {
		String bandId, dataUrl;
		
		_logger.info("bandIdToFilter: <"+bandIdToFilter.toString()
					+ "> Filters All < " + Filters.ALL 
					+ "> " );
		Filter bandFilter = bandIdToFilter.equals("")? Filters.ALL : Filters.eq("bandId", bandIdToFilter);
		NitriteCollection dataCollection = _db.getCollection(BANDS_COLLECTION);
		Cursor dataCursor = (bandFilter != null) ? dataCollection.find(bandFilter) : dataCollection.find();
		JSONArray jsonArray = new JSONArray();
		
		_logger.info("Found <"+dataCursor.size() + "> bands registered");
		
		for (Document dataDocument: dataCursor) {
			bandId = (String)dataDocument.get("bandId");
			dataUrl = (String)dataDocument.get("dataUrl");
			_logger.info("Band: <"+bandId+"> Url <"+dataUrl+">");
			
			jsonArray.put(getBandJsonObject(bandId, dataUrl));
		}
		
		return jsonArray.toString();
	}
	
	
	public String getServicesInJson() {
		String serviceUrl;
		JSONArray jsonArray = new JSONArray();
		Filter dataFilter = Filters.ALL;
		NitriteCollection dataCollection = _db.getCollection(SERVICES_COLLECTION);
//		Cursor dataCursor = (dataFilter != null) ? dataCollection.find(dataFilter) : dataCollection.find();
		Cursor dataCursor = dataCollection.find();
		
		_logger.info("Found <"+dataCursor.size()+"> Device Services method");
		for(Document dataDocument : dataCursor) {
			
			serviceUrl = (String)dataDocument.get("serviceUrl");
			_logger.info("serviceUrl "+dataDocument.toString());

			jsonArray.put(getServicesJsonObject(serviceUrl));
		}
		
		
		return jsonArray.toString(); 
	}
	
	public String getAllDataInJson() {
		return getDataInJson(Filters.ALL);
	}
	
	public String getDataByDeviceIdInJson(String deviceIdToFilter) {
		Filter dataFilter = Filters.eq("deviceId", deviceIdToFilter);
		
		
		return getDataInJson(dataFilter);
	}
	
	public String getDataByBandIdInJson(String bandIdToFilter) {
		Filter dataFilter = Filters.eq("bandId", bandIdToFilter);
		
		
		return getDataInJson(dataFilter);
	}
	

	
	public String getDataByTimestampInJson(String timestampToFilter) {
		Filter dataFilter = Filters.gte("timestamp", timestampToFilter);
		Filter dataToRemoveFilter = Filters.lte("timestamp", timestampToFilter);
		
		_logger.info("Getting data starting in: <"+timestampToFilter+">");
		
//		removeData(dataToRemoveFilter);
		
		return getDataInJson(dataFilter);
	}
	
	public String getDataByTimestampAndBandIdInJson(String bandIdToFilter, String timestampToFilter) {
		Filter bandFilter = Filters.eq("bandId", bandIdToFilter);
//		Filter timestampFilter = Filters.gte("timestamp", timestampToFilter);
		Filter timestampFilter = Filters.eq("timestamp", timestampToFilter);
		Filter dataFilter = Filters.and(bandFilter, timestampFilter);
		
		_logger.info("Filtering <bandId:"+bandIdToFilter+"> & <timestamp:"+timestampToFilter+">");
		
		return getDataInJson(dataFilter);
	}
	
	private String getDataInJson(Filter dataFilter) {
		JSONObject jsonObject;
		JSONArray jsonArray = new JSONArray();
		String deviceId, dataUrl, dataType, parsedData, timestamp;
		NitriteCollection dataCollection = _db.getCollection(DATA_COLLECTION);
		Cursor dataCursor = (dataFilter != null) ? dataCollection.find(dataFilter) : dataCollection.find();
		
		_logger.info("Found <"+dataCursor.size()+"> data records...");
		
		for(Document dataDocument : dataCursor) {
			deviceId = (String)dataDocument.get("deviceId");
			dataUrl = (String)dataDocument.get("dataUrl");
			dataType = (String)dataDocument.get("dataType");
			parsedData = (String)dataDocument.get("parsedData");
			timestamp = (String)dataDocument.get("timestamp");
			
			jsonObject = getDataJsonObject(deviceId, dataUrl, dataType, parsedData, timestamp);
			jsonArray.put(jsonObject);
		}
		
		return jsonArray.toString();
	}
	
//	private String getBandsInJson() {
//		
//	}
	
	private JSONObject getDataJsonObject(String deviceId, String dataUrl, String dataType, String parsedData, String timestamp) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("deviceId", deviceId);
		jsonObject.put("dataUrl", dataUrl);
		jsonObject.put("dataType", dataType);
		jsonObject.put("parsedData", parsedData);
		jsonObject.put("timestamp", timestamp);
		
		return jsonObject;
	}
	
	private JSONObject getBandJsonObject(String bandId, String dataUrl) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("bandId", bandId);
		jsonObject.put("dataUrl", dataUrl);
		return jsonObject;
	}
	
	private JSONObject getServicesJsonObject(String serviceUrl) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("serviceUrl", serviceUrl);
		return jsonObject;
	}
	
	private void removeData(Filter dataFilter) {
		NitriteCollection dataCollection = _db.getCollection(DATA_COLLECTION);
		dataCollection.remove(dataFilter);
		_logger.info("Removed data");
	}
	
	/* -------------------------------- INSERT METHODS -------------------------------- */
	public boolean insertBandDocument(String bandId, String dataUrl) {
		if(!hasBandInDb(bandId)) {
			Document document = createBandDocument(bandId, dataUrl);
			NitriteCollection collection = _db.getCollection(BANDS_COLLECTION);
			
			collection.insert(document);
			_logger.info("Added band " + bandId);
			
			return true;
		}
		
		_logger.warn("Band " + bandId + " already exists");
		return false;
	}
	
	/**
	 * It is used the device address, because we are not interested in getting the protocol
	 * and the adapter address
	 * 
	 * @param device
	 */
	public boolean insertDeviceDocument(DiscoveredDevice device) {
		String deviceUrl = device.getURL().getDeviceAddress();
		
		if(!hasDeviceInDb(deviceUrl)) {
			String deviceName = device.getName();
			Document document = createDeviceDocument(deviceName, deviceUrl);
			NitriteCollection collection = _db.getCollection(DEVICES_COLLECTION);
			
			collection.insert(document);
			_logger.info("Added device " + deviceUrl);
			
			return true;
		} else {
			_logger.warn("Device " + deviceUrl + " already exists");
			return false;
		}
	}
	
	public boolean insertServiceDocument(GattService service) {
		String serviceUrl = service.getURL().getDeviceAddress() + "/" + service.getURL().getServiceUUID();
		
		if(!hasServiceInDb(serviceUrl)) {
			Document document = createServiceDocument(serviceUrl);
			NitriteCollection collection = _db.getCollection(SERVICES_COLLECTION);
			
			collection.insert(document);
			_logger.info("Added service " + serviceUrl);
			
			return true;
		} else {
			_logger.warn("Service " + serviceUrl + " already exists");
			
			return false;
		}
	}
	
	public boolean insertCharacteristicDocument(String characteristicUrl, Characteristic characteristic) {
		if(!hasCharacteristicInDb(characteristicUrl)) {
			Document document = createCharacteristicDocument(characteristicUrl, characteristic);
			NitriteCollection collection = _db.getCollection(CHARACTERISTICS_COLLECTION);
			
			collection.insert(document);
			_logger.info("Added characteristic " + characteristicUrl);
			
			return true;
		} else {
			_logger.warn("Characteristic " + characteristicUrl + " already exists");
			return false;
		}
	}
	
	public boolean insertFieldDocument(byte[] rawData, String fieldName, String characteristicUrl) {
		String fieldUrl = characteristicUrl + "/" + fieldName;
		
		if(!hasFieldInDb(fieldUrl)) {
			Document document = createFieldDocument(rawData, fieldName, fieldUrl);
			NitriteCollection collection = _db.getCollection(FIELDS_COLLECTION);
			
			collection.insert(document);
			_logger.info("Added field " + fieldUrl);
			
			return true;
		} else {
			_logger.warn("Field " + fieldUrl + " already exists");
			return false;
		}
	}
	
	public void insertDataDocument(Band band, String parsedData) {
		String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		Document document = createDataDocument(band, parsedData, timestamp);
		NitriteCollection collection = _db.getCollection(DATA_COLLECTION);
		
		collection.insert(document);
//		_logger.info("Added data: [" + parsedData + ", " + band.getBandId() + "]");
	}
	
	/*public void insertDataDocument(String parsedData, String fieldUrl) {
		String timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		
		if(canAddNewData(timestamp, fieldUrl)) {
			Document document = createDataDocument(parsedData, fieldUrl, timestamp);
			NitriteCollection collection = _db.getCollection(DATA_COLLECTION);
			
			collection.insert(document);
			_logger.info("Added data: [" + parsedData + ", " + fieldUrl + "]");
		}
	}*/
	
	/* -------------------------------- CREATE METHODS -------------------------------- */
	private Document createBandDocument(String bandId, String dataUrl) {
		Document document = new Document();
		document.put("bandId", bandId);
		document.put("dataUrl", dataUrl);
		
		return document;
	}
	
	private Document createDeviceDocument(String deviceName, String deviceUrl) {
		Document document = new Document();
		document.put("deviceName", deviceName);
		document.put("deviceUrl", deviceUrl);
		
		return document;
	}
	
	private Document createServiceDocument(String serviceUrl) {
		Document document = new Document();
		document.put("serviceUrl", serviceUrl);
		
		return document;
	}
	
	private Document createCharacteristicDocument(String characteristicUrl, Characteristic characteristic) {
		String characteristicName = characteristic.getName();
		boolean isReadable = characteristic.isValidForRead();
		boolean isWritable = characteristic.isValidForWrite();
		Document document = new Document();
		
		document.put("characteristicName", characteristicName);
		document.put("characteristicUrl", characteristicUrl);
		document.put("isReadable", isReadable);
		document.put("isWritable", isWritable);
		
		return document;
	}
	
	private Document createFieldDocument(byte[] rawData, String fieldName, String fieldUrl) {
		Document document = new Document();
		document.put("fieldName", fieldName);
		document.put("rawData", rawData);
		document.put("fieldUrl", fieldUrl);
		
		return document;
	}
	
	private Document createDataDocument(Band band, String parsedData, String timestamp) {
		Document document = new Document();
		document.put("bandId", band.getBandId());
		document.put("dataUrl", band.getDataUrl());
		document.put("parsedData", parsedData);
		document.put("timestamp", timestamp);
		
		return document;
	}
	
	/*private Document createDataDocument(String parsedData, String fieldUrl, String timestamp) {
		Document document = new Document();
		document.put("parsedData", parsedData);
		document.put("fieldUrl", fieldUrl);
		document.put("timestamp", timestamp);
		
		return document;
	}*/

	/* -------------------------------- UPDATE METHODS -------------------------------- */
	public boolean updateDeviceDocument(String deviceId, boolean read) {
		if(hasDeviceInDb(deviceId)) {
			NitriteCollection deviceCollection = _db.getCollection(DEVICES_COLLECTION);
			Cursor deviceCursor = deviceCollection.find(Filters.eq("deviceId", deviceId));

			if(deviceCursor.size() == 1) {
				for(Document deviceDocument : deviceCursor) {
					deviceDocument.put("read", read);
					deviceCollection.update(deviceDocument);
				}
				
				_logger.info("Updated device " + deviceId + " readability to " + read);
				return true;
			}
		}
		
		_logger.warn("Device " + deviceId + " does not exist");
		return false;
	}
	
	public void updateAllDeviceDocuments(boolean read) {
		NitriteCollection deviceCollection = _db.getCollection(DEVICES_COLLECTION);
		Cursor deviceCursor = deviceCollection.find();

		for(Document deviceDocument : deviceCursor) {
			deviceDocument.put("read", read);
			deviceCollection.update(deviceDocument);
		}
		
		_logger.info("Updated all devices' readability to " + read);
	}
	
	
	/* ----------------------------- AUXILIARY DATA METHODS ---------------------------- */
	private boolean canAddNewData(String timestamp, String fieldUrl) {
		NitriteCollection dataCollection = _db.getCollection(DATA_COLLECTION);
		FindOptions dataFindOptions = FindOptions.sort("timestamp", SortOrder.Descending);
		Filter dataFilter = Filters.eq("fieldUrl", fieldUrl);
		Cursor dataCursor = dataCollection.find(dataFilter, dataFindOptions);
		
		if(dataCursor.size() == 0) { return true; }
		
		if(dataCursor.size() > 0) {
			Document dataDocument = dataCursor.firstOrDefault();
			String timestampToCompare = (String)dataDocument.get("timestamp");
			
			if(hasNewData(timestamp, timestampToCompare)) { return true; }
		}
		
		return false;
	}
	
	// 0123456789012345678
	// yyyy-MM-dd HH:mm:ss
	// Checks if the new data received is not from the same minute to set one minute interval between
	// readings to be stored
	private boolean hasNewData(String timestamp1, String timestamp2) {
		int year1 = Integer.parseInt(timestamp1.substring(0, 4));
		int month1 = Integer.parseInt(timestamp1.substring(5, 7));
		int day1 = Integer.parseInt(timestamp1.substring(8, 10));
		int hour1 = Integer.parseInt(timestamp1.substring(11, 13));
		int minute1 = Integer.parseInt(timestamp1.substring(14, 16));
		
		int year2 = Integer.parseInt(timestamp2.substring(0, 4));
		int month2 = Integer.parseInt(timestamp2.substring(5, 7));
		int day2 = Integer.parseInt(timestamp2.substring(8, 10));
		int hour2 = Integer.parseInt(timestamp2.substring(11, 13));
		int minute2 = Integer.parseInt(timestamp2.substring(14, 16));
		
		_logger.info("Timestamp1: " + year1 + "-" + month1 + "-" + day1 + " " + hour1 + ":" + minute1);
		_logger.info("Timestamp2: " + year2 + "-" + month2 + "-" + day2 + " " + hour2 + ":" + minute2);
		
		if((year1 == year2) && (month1 == month2) && (day1 == day2) && (hour1 == hour2) && (minute1 == minute2)) {
			return false;
		} else if((year1 == year2) && (month1 == month2) && (day1 == day2) && (hour1 == hour2) && (minute1 > minute2)) {
			return true;
		} else if((year1 == year2) && (month1 == month2) && (day1 == day2) && (hour1 > hour2)) {
			return true;
		} else if((year1 == year2) && (month1 == month2) && (day1 > day2)) {
			return true;
		} else if((year1 == year2) && (month1 > month2)) {
			return true;
		} else if(year1 > year2) {
			return true;
		}
		
		return false;
	}
	
	/* ------------------------------ HAS ELEMENT METHODS ------------------------------ */
	private boolean hasBandInDb(String bandId) {
		return hasElementInDb(BANDS_COLLECTION, "bandId", bandId);
	}
	
	private boolean hasDeviceInDb(String deviceUrl) {
		return hasElementInDb(DEVICES_COLLECTION, "deviceUrl", deviceUrl);
	}	
	
	private boolean hasServiceInDb(String serviceUrl) {
		return hasElementInDb(SERVICES_COLLECTION, "serviceUrl", serviceUrl);
	}
	
	private boolean hasCharacteristicInDb(String characteristicUrl) {
		return hasElementInDb(CHARACTERISTICS_COLLECTION, "characteristicUrl", characteristicUrl);
	}
	
	private boolean hasFieldInDb(String fieldUrl) {
		return hasElementInDb(FIELDS_COLLECTION, "fieldUrl", fieldUrl);
	}
	
	private boolean hasElementInDb(String collectionName, String fieldToEvaluate, String elementUrl) {
		NitriteCollection collection = _db.getCollection(collectionName);
		Cursor cursor = collection.find(Filters.eq(fieldToEvaluate, elementUrl));
		
		return (cursor.size() > 0);
	}

	/* ---------------------------- BIND ELEMENTS METHODS ----------------------------- */
	/**
	 * The regular expression in the serviceFilter looks for serviceUrl which contains the
	 * deviceUrl in its beginning
	 * 
	 * @param deviceUrl
	 */
	public void addServicesToDevice(String deviceUrl) {
		if(hasDeviceInDb(deviceUrl)) {
			NitriteCollection deviceCollection = _db.getCollection(DEVICES_COLLECTION);
			Cursor deviceCursor = deviceCollection.find(Filters.eq("deviceUrl", deviceUrl));

			if(deviceCursor.size() == 1) {
				NitriteCollection serviceCollection = _db.getCollection(SERVICES_COLLECTION);
				Filter serviceFilter = Filters.regex("serviceUrl", "^(" + deviceUrl + ").*");
				Cursor serviceCursor = serviceCollection.find(serviceFilter);
				ArrayList<Long> services = new ArrayList<Long>();
				
				for(Document serviceDocument : serviceCursor) {
					Long serviceId = new Long((long)serviceDocument.get("_id"));
					services.add(serviceId);
				}
				
				Filter deviceFilter = Filters.eq("deviceUrl", deviceUrl);
				Document documentToUpdate = Document.createDocument("services", services);
				
				deviceCollection.update(deviceFilter, documentToUpdate);
				
				_logger.info("Services added to the device " + deviceUrl);
			} else {
				_logger.error("There is more than one device in DB with " + deviceUrl);
			}
		} else {
			_logger.warn("The device " + deviceUrl + " doesn't exist, so it is impossible to add services to it");
		}
	}

	/**
	 * The regular expression in the characteristicFilter looks for characteristicUrlUrl 
	 * which contains the serviceUrl in its beginning
	 * 
	 * @param serviceUrl
	 */
	public void addCharacteristicsToService(String serviceUrl) {
		if(hasServiceInDb(serviceUrl)) {
			NitriteCollection serviceCollection = _db.getCollection(SERVICES_COLLECTION);
			Cursor serviceCursor = serviceCollection.find(Filters.eq("serviceUrl", serviceUrl));

			if(serviceCursor.size() == 1) {
				NitriteCollection characteristicCollection = _db.getCollection(CHARACTERISTICS_COLLECTION);
				Filter characteristicFilter = Filters.regex("characteristicUrl", "^(" + serviceUrl + ").*");
				Cursor characteristicCursor = characteristicCollection.find(characteristicFilter);
				ArrayList<Long> characteristics = new ArrayList<Long>();
				
				for(Document characteristicDocument : characteristicCursor) {
					Long characteristicId = new Long((long)characteristicDocument.get("_id"));
					characteristics.add(characteristicId);
				}
				
				Filter serviceFilter = Filters.eq("serviceUrl", serviceUrl);
				Document documentToUpdate = Document.createDocument("characteristics", characteristics);
				
				serviceCollection.update(serviceFilter, documentToUpdate);
				
				_logger.info("Characteristics added to the service " + serviceUrl);
			} else {
				_logger.error("There is more than one service in DB with " + serviceUrl);
			}
		} else {
			_logger.warn("The service " + serviceUrl + " doesn't exist, so it is impossible to add characteristics to it");
		}
	}
	
	public void addFieldsToCharacteristic(String characteristicUrl) {
		if(hasCharacteristicInDb(characteristicUrl)) {
			NitriteCollection characteristicCollection = _db.getCollection(CHARACTERISTICS_COLLECTION);
			Cursor characteristicCursor = characteristicCollection.find(Filters.eq("characteristicUrl", characteristicUrl));

			if(characteristicCursor.size() == 1) {
				NitriteCollection fieldCollection = _db.getCollection(FIELDS_COLLECTION);
				Filter fieldFilter = Filters.regex("fieldUrl", "^(" + characteristicUrl + ").*");
				Cursor fieldCursor = fieldCollection.find(fieldFilter);
				ArrayList<Long> fields = new ArrayList<Long>();
				
				for(Document fieldDocument : fieldCursor) {
					Long fieldId = new Long((long)fieldDocument.get("_id"));
					fields.add(fieldId);
				}
				
				Filter characteristicFilter = Filters.eq("characteristicUrl", characteristicUrl);
				Document documentToUpdate = Document.createDocument("fields", fields);
				
				characteristicCollection.update(characteristicFilter, documentToUpdate);
				
				_logger.info("Fields added to the characteristic " + characteristicUrl);
			} else {
				_logger.error("There is more than one characteristic in DB with " + characteristicUrl);
			}
		} else {
			_logger.warn("The characteristic " + characteristicUrl + " doesn't exist, so it is impossible to add fields to it");
		}
	}
	
	public void addDataToField(String fieldUrl) {
		if(hasFieldInDb(fieldUrl)) {
			NitriteCollection fieldCollection = _db.getCollection(FIELDS_COLLECTION);
			Cursor fieldCursor = fieldCollection.find(Filters.eq("fieldUrl", fieldUrl));

			if(fieldCursor.size() == 1) {
				NitriteCollection dataCollection = _db.getCollection(DATA_COLLECTION);
				Filter dataFilter = Filters.eq("fieldUrl", fieldUrl);
				Cursor dataCursor = dataCollection.find(dataFilter);
				ArrayList<Long> data = new ArrayList<Long>();
				
				for(Document dataDocument : dataCursor) {
					Long dataId = new Long((long)dataDocument.get("_id"));
					data.add(dataId);
				}
				
				Filter fieldFilter = Filters.eq("fieldUrl", fieldUrl);
				Document documentToUpdate = Document.createDocument("data", data);
				
				fieldCollection.update(fieldFilter, documentToUpdate);
				
				_logger.info("Data added to the field " + fieldUrl);
			} else {
				_logger.error("There is more than one field in DB with " + fieldUrl);
			}
		} else {
			_logger.warn("The field " + fieldUrl + " doesn't exist, so it is impossible to add data to it");
		}
	}
}

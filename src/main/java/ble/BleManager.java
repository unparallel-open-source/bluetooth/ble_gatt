package ble;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.sputnikdev.bluetooth.URL;
import org.sputnikdev.bluetooth.gattparser.BluetoothGattParser;
import org.sputnikdev.bluetooth.gattparser.BluetoothGattParserFactory;
import org.sputnikdev.bluetooth.gattparser.FieldHolder;
import org.sputnikdev.bluetooth.gattparser.GattResponse;
import org.sputnikdev.bluetooth.gattparser.spec.Characteristic;
import org.sputnikdev.bluetooth.gattparser.spec.Field;
import org.sputnikdev.bluetooth.gattparser.spec.Value;
import org.sputnikdev.bluetooth.manager.BluetoothManager;
import org.sputnikdev.bluetooth.manager.CharacteristicGovernor;
import org.sputnikdev.bluetooth.manager.DeviceGovernor;
import org.sputnikdev.bluetooth.manager.DiscoveredAdapter;
import org.sputnikdev.bluetooth.manager.DiscoveredDevice;
import org.sputnikdev.bluetooth.manager.GattCharacteristic;
import org.sputnikdev.bluetooth.manager.GattService;
import org.sputnikdev.bluetooth.manager.impl.BluetoothManagerBuilder;

import storage.DbOperations;
import utils.CallbackListener;
import utils.OSInfo;
import utils.Print;
import utils.*;

public class BleManager {

	private final Logger _logger;
	private OSInfo _osInfo;
	private static DbOperations _db;
	private BluetoothManagerBuilder _bleManagerBuilder;
	private static BluetoothManager _bleManager;
	private ArrayList<CallbackListener> _callbackListeners;
	private BluetoothGattParser _gattParser;
	private ArrayList<DiscoveredDevice> _discoveredDevices;
	private Map<DiscoveredDevice, ArrayList<GattService>> _gattServicesByDevice;
	//private Map<GattService, ArrayList<GattCharacteristic>> _gattCharacteristicByService;
	private Map<URL, Characteristic> _gattCharacteristicByUrl;
	private Map<Characteristic, ArrayList<Field>> _gattFieldsByCharacteristic;
	
	public BleManager(boolean start, boolean activateDiscovering, int discoveryRate, int refreshRate, boolean rediscover) {
		_logger = Logger.getLogger(BleManager.class);
		_osInfo = new OSInfo();
		_db = new DbOperations();
		_callbackListeners = new ArrayList<CallbackListener>();
		_gattParser = BluetoothGattParserFactory.getDefault();
		_discoveredDevices = new ArrayList<DiscoveredDevice>();
		_gattServicesByDevice = new HashMap<DiscoveredDevice, ArrayList<GattService>>();
		//_gattCharacteristicByService = new HashMap<GattService, ArrayList<GattCharacteristic>>();
		_gattCharacteristicByUrl = new HashMap<URL, Characteristic>();
		_gattFieldsByCharacteristic = new HashMap<Characteristic, ArrayList<Field>>();
		
		buildBleManager(start, activateDiscovering, discoveryRate, refreshRate, rediscover);
	}
	
	/**
	 * 
	 * @param start if true, BLE manager will start
	 * @param activateDiscovering if true, the discovery process will be enabled
	 * @param discoveryRate
	 * @param refreshRate
	 * @param rediscover
	 */
	public void buildBleManager(boolean start, boolean activateDiscovering, int discoveryRate, int refreshRate, boolean rediscover) {
		setBleManagerConfiguration(start, activateDiscovering, discoveryRate, refreshRate, rediscover);
		_bleManager = _bleManagerBuilder.build();
	}
	
	public void startDiscovery() {
		if(_bleManager != null) {
			if(!_bleManager.isStarted()) {
				_bleManager.start(true);
			}
		}
	}
	
	public static BluetoothManager getBleManager() {
		return _bleManager;
	}
	
	private void setBleManagerConfiguration(boolean start, boolean activateDiscovering, int discoveryRate, int refreshRate, boolean rediscover) {
		_bleManagerBuilder = new BluetoothManagerBuilder();
		
		_bleManagerBuilder.withStarted(start);
		_bleManagerBuilder.withDiscovering(activateDiscovering);
		_bleManagerBuilder.withIgnoreTransportInitErrors(true);
		setBleManagerTransportType();
		setBleManagerDiscoveryConfiguration(discoveryRate, refreshRate, rediscover);
	}
	
	/**
	 * If <code>discoveryRate</code> or <code>refreshRate</code> is equal to <code>-1</code>, the
	 * previous value will be unchanged (if the value was never changed, those variables will have
	 * the default value).
	 * 
	 * @param discoveryRate in seconds
	 * @param refreshRate in seconds
	 * @param rediscover
	 */
	private void setBleManagerDiscoveryConfiguration(int discoveryRate, int refreshRate, boolean rediscover) {
		if(discoveryRate != -1) {
			_bleManagerBuilder.withDiscoveryRate(discoveryRate);
		}
		
		if(refreshRate != -1) {
			_bleManagerBuilder.withRefreshRate(refreshRate);
		}
		
		_bleManagerBuilder.withRediscover(rediscover);
		
		//_logger.info("Discovery configuration was set");
	}
	
	private void setBleManagerTransportType() {
		OSInfoEnum osType = _osInfo.getOsTypeEnum();

		switch(osType) {
			case LINUX: {
				_bleManagerBuilder.withTinyBTransport(true);
				//_bleManagerBuilder.withBlueGigaTransport("(/dev/ttyACM)[0-9]{1,3}");
				break;
			}
			case WINDOWS: {
				_bleManagerBuilder.withBlueGigaTransport("(COM)[0-9]{1,3}");
				break;
			}
			case MACOS: {
				_bleManagerBuilder.withBlueGigaTransport("/dev/tty.(usbmodem).*");
				break;
			}
			case UNKNOWN:
			default: {
				Print.printError("Unknown OS: TinyB and BlueGiga Transport not set");
				break;
			}
		}
		
		//_logger.info("Operative System: " + osType);
	}
	
	/**
	 * @deprecated Use getOsTypeEnum from OSInfo class
	 * @return
	 */
	@Deprecated
	private int getOsType() {
		return _osInfo.getOsType();
	}
	
	/* ------------------------------- CALLBACK METHODS ------------------------------- */
	
	public void addCallbackListener(CallbackListener callbackListener) {
		_callbackListeners.add(callbackListener);
	}
	
	public void setDeviceListenerScheduler(DiscoveredDevice discoveredDevice) {
		for(CallbackListener callbackListener : _callbackListeners) {
			callbackListener.setDeviceListenerScheduler(discoveredDevice);
		}
	}
	
	public void notifyDevicesUpdate(DiscoveredDevice discoveredDevice) {
		for(CallbackListener callbackListener : _callbackListeners) {
			callbackListener.updateDiscoveredDevices(discoveredDevice);
		}
	}
	
	public void notifyResolvedServicesUpdate(URL deviceUrl, ArrayList<GattService> gattServices) {
		for(CallbackListener callbackListener : _callbackListeners) {
			for(GattService gattService : gattServices) {
				callbackListener.updateResolvedServices(deviceUrl.getDeviceAddress(), gattService);
			}
		}
	}
	
	public void notifyCharacteristicsUpdate(GattService gattService) {
		for(CallbackListener callbackListener : _callbackListeners) {
			for(URL characteristicUrl : _gattCharacteristicByUrl.keySet()) {
				callbackListener.updateCharacteristics(gattService, characteristicUrl);
			}
		}
	}
	
	public void notifyRawDataReady(byte[] rawData, String fieldName, String characteristicUrl) {
		for(CallbackListener callbackListener : _callbackListeners) {
			callbackListener.updateRawData(rawData, fieldName, characteristicUrl);
		}
	}
	
	public void notifyParsedDataReady(FieldHolder fieldHolder, String fieldName, String characteristicUrl) {
		String parsedData = fieldHolder.toString();
		String fieldUrl = characteristicUrl + "/" + fieldName;
		
		for(CallbackListener callbackListener : _callbackListeners) {
			callbackListener.updateParsedData(parsedData, fieldUrl);
		}
	}
	
	/* -------------------------------- DEVICE METHODS -------------------------------- */
	
	public void discoverDevices() {
		_bleManager.addDeviceDiscoveryListener(discoveredDevice -> {
			
			String x = "Discovered device <"+discoveredDevice.getDisplayName()+"> "
 								    + "or <"+discoveredDevice.getName()+"> "
								    + "or <"+discoveredDevice.getAlias()+"> "
						  	    + "signal <"+discoveredDevice.getRSSI()+" db> "
				  		       + "Address <"+discoveredDevice.getURL().getDeviceAddress()+">" 
				  		      + "Protocol <"+discoveredDevice.getURL().getProtocol()+">" 
				    + "CharacteristicUUID <"+discoveredDevice.getURL().getCharacteristicUUID()+">" 
				  		        + "Keymap <";
			
//			discoveredDevice.getURL().getDeviceAttributes().forEach((k,v)->System.out.println("["+k+":"+v+"]"));
//			discoveredDevice.getURL().getDeviceAttributes().entrySet();
			String attribs = "";
			for (Map.Entry<String, String> DeviceAttribute : discoveredDevice.getURL().getDeviceAttributes().entrySet() ) {
				attribs += "["+DeviceAttribute.getKey()+":"+DeviceAttribute.getValue()+"]";
			}
			_logger.info(x+attribs+">"
					+ "Comp Addr <"+discoveredDevice.getURL().getDeviceCompositeAddress()+">");
			
			
			
			
			
			discoverResolvedServicesByDevice(discoveredDevice);

			
			
			
			if(/*discoveredDevice.getURL().getDeviceAddress().equals("EB:D8:42:9B:5C:D1""5E:D8:62:CE:24:D8""A0:9E:1A:2C:7C:A4")
			   || discoveredDevice.getURL().getDeviceAddress().equals("A0:9E:1A:2C:7C:A4") 
			   ||*/ discoveredDevice.getURL().getDeviceAddress().equals("A0:9E:1A:2C:7C:A4")) {
				if(!hasDevice(discoveredDevice)) {
					if(_discoveredDevices.add(discoveredDevice)) {
						notifyDevicesUpdate(discoveredDevice);
						setDeviceListenerScheduler(discoveredDevice);
						_logger.info("New: [" + discoveredDevice.getName() + ", " + discoveredDevice.getURL() + "]");	
					}
				}
			} else if (discoveredDevice.getURL().getDeviceAddress().equals("EB:D8:42:9B:5C:D1")) {
				if(!hasDevice(discoveredDevice)) {
					if(_discoveredDevices.add(discoveredDevice)) {
						notifyDevicesUpdate(discoveredDevice);
						setDeviceListenerScheduler(discoveredDevice);
						_logger.info("New: [" + discoveredDevice.getName() + ", " + discoveredDevice.getURL() + "]");	
					}
				}
			}
		});
	}
	
	private boolean hasDevice(DiscoveredDevice discoveredDevice) {
		return _discoveredDevices.contains(discoveredDevice);
	}
	
	public ArrayList<DiscoveredDevice> getDiscoveredDevices() {
		return _discoveredDevices;
	}
	
	public void removeInactiveDevice(DiscoveredDevice deviceToRemove) {
		if(deviceToRemove != null) {
			if(_discoveredDevices.remove(deviceToRemove)) {
				_logger.info("Removed: [" + deviceToRemove.getName() + ", " + deviceToRemove.getURL() + "]");
			}
		}
	}
	
	public void listDiscoveredDevices(ArrayList<DiscoveredDevice> discoveredDevices) {
		Print.printAllDevicesInfo(discoveredDevices);
	}
	
	/* ------------------------------- SERVICE METHODS -------------------------------- */
	
	public void discoverResolvedServicesByDevice(DiscoveredDevice device) {
		URL deviceUrl = device.getURL();
		DeviceGovernor deviceGovernor = _bleManager.getDeviceGovernor(deviceUrl, true);
		ArrayList<GattService> gattServices = new ArrayList<GattService>();
		
		if(isDeviceGovernorValid(deviceGovernor)) {
			deviceGovernor.whenServicesResolved(DeviceGovernor::getResolvedServices).thenAccept(services -> {
				if(!_gattServicesByDevice.containsKey(device)) {
					_logger.info("New service(s) for [" + device.getName() + ", " + device.getURL().getDeviceAddress() + "]");
					String servicesStr = "";
					for (int l = 0; l<services.size();l++) {
						servicesStr += "["+services.get(l).getURL().getCharacteristicUUID()+"]";
					}
					_logger.info("Service list: "+servicesStr);
					gattServices.addAll(services);
					_gattServicesByDevice.put(device, gattServices);
					//listResolvedServicesByDevice(device);
					notifyResolvedServicesUpdate(deviceUrl, gattServices);
				} else {
					//_logger.warn("Device [" + device.getName() + ", " + device.getURL() + "] already exists");
				}
			});
		}
	}
	
	public DeviceGovernor getDeviceGovernor(URL deviceUrl) {
		return _bleManager.getDeviceGovernor(deviceUrl, true);
	}
	
	private boolean isDeviceGovernorValid(DeviceGovernor deviceGovernor) {
		if(deviceGovernor != null) {
			if(deviceGovernor.isBleEnabled()) {
				return true;
			}
		} else {
			_logger.error("DeviceGovernor is NULL");
		}
		
		return false;
	}
	
	public Map<DiscoveredDevice, ArrayList<GattService>> getResolvedServices() {
		return _gattServicesByDevice;
	}
	
	public void listResolvedServicesByDevice(DiscoveredDevice device) {
		ArrayList<GattService> gattServices = _gattServicesByDevice.get(device);
		
		_logger.info("List of Services for device [" + device.getURL() + "]:");
		Print.printAllServicesInfo(gattServices);
	}
	
	public boolean isKnownService(String serviceUuid) {
		return _gattParser.isKnownService(serviceUuid);
	}
	
	/* --------------------------- CHARACTERISTIC METHODS ---------------------------- */
	
	public void discoverCharacteristicsByServiceByDevice(GattService gattService) {
		ArrayList<GattCharacteristic> gattCharacteristics = new ArrayList<GattCharacteristic>();
		gattCharacteristics.addAll(gattService.getCharacteristics());
		
		buildCharacteristicByUrlMap(gattCharacteristics);
		notifyCharacteristicsUpdate(gattService);
	}
	
	public Characteristic getCharacteristicByUrl(URL characteristicUrl) {
		if(characteristicUrl != null) {
			for(URL url : _gattCharacteristicByUrl.keySet()) {
				if(url.equals(characteristicUrl)) {
					return _gattCharacteristicByUrl.get(url);
				}
			}
		}
		
		return null;
	}
	
	/**
	 * REVIEW METHOD NAME
	 * 
	 * @param gattCharacteristics
	 */
	private void buildCharacteristicByUrlMap(ArrayList<GattCharacteristic> gattCharacteristics) {
		Characteristic characteristic;
		URL characteristicUrl;
		
		for(GattCharacteristic gattCharacteristic : gattCharacteristics) {
			characteristicUrl = gattCharacteristic.getURL();
			if(!_gattCharacteristicByUrl.containsKey(characteristicUrl)) {
				characteristic = castGattCharacteristicToCharacteristic(gattCharacteristic);
				_gattCharacteristicByUrl.put(characteristicUrl, characteristic);
			} else {
				  //_logger.warn("CharacteristicByUrl Map already contains " + characteristicUrl);
			}
		}
	}
	
	private Characteristic castGattCharacteristicToCharacteristic(GattCharacteristic gattCharacteristic) {
		URL characteristicUrl = gattCharacteristic.getURL();
		String characteristicUuid = characteristicUrl.getCharacteristicUUID();
		Characteristic characteristic = _gattParser.getCharacteristic(characteristicUuid);
		
		return characteristic;
	}
	
	public void listCharacteristicsByServiceByDevice(Map<DiscoveredDevice, ArrayList<GattService>> gattServicesByDevice) {
		for(DiscoveredDevice device : gattServicesByDevice.keySet()) {
			for(GattService service : gattServicesByDevice.get(device)) {
				listCharacteristicsByService(service);
			}
		}
	}
	
	private void listCharacteristicsByService(GattService gattService) {
		ArrayList<GattCharacteristic> gattCharacteristics = new ArrayList<GattCharacteristic>();
		gattCharacteristics.addAll(gattService.getCharacteristics());
		
		Print.printAllCharacteristicsInfo(gattCharacteristics);
	}
	
/*	public void parseData(URL characteristicUrl) {
		CharacteristicGovernor characteristicGovernor = _bleManager.getCharacteristicGovernor(characteristicUrl);

		characteristicGovernor.whenReady(CharacteristicGovernor::isReadable).thenAccept(readable -> {
			if(readable) {
				_logger.warn("The Characteristic with URL " + characteristicUrl + " is readable");
				
				characteristicGovernor.whenReady(CharacteristicGovernor::read).thenAccept(rawData -> {
					Characteristic characteristic = _gattCharacteristicByUrl.get(characteristicUrl);
					
					if(characteristic != null) {
						if(!_gattFieldsByCharacteristic.containsKey(characteristic)) {
							buildFieldsByCharacteristicMap(characteristic);

							FieldHolder fieldHolder;
							String fieldName;
							String serviceUrl = characteristicUrl.getDeviceAddress() + "/" + characteristicUrl.getServiceUUID();
							String characUrl = serviceUrl + "/" + characteristicUrl.getCharacteristicUUID();
	
							for(Field field : _gattFieldsByCharacteristic.get(characteristic)) {
								fieldName = field.getName();
								fieldHolder = getParsedData(rawData, fieldName, characteristic);
								
								notifyRawDataReady(rawData, fieldName, characUrl);
								notifyParsedDataReady(fieldHolder, fieldName, characUrl);
							}
						} else {
							  //_logger.warn("FieldsByCharacteristic Map already contains " + characteristic.getName());
						}
					} else {
						//_logger.warn("There is no characteristic with URL " + characteristicUrl);
					}
			    });
			} else {
				_logger.warn("The Characteristic with URL " + characteristicUrl + " is NOT readable");
				
				characteristicGovernor.whenReady(CharacteristicGovernor::isNotifiable).thenAccept(notifiable -> {
					if(notifiable) {
						characteristicGovernor.addValueListener(notificationData -> {
							Characteristic characteristic = _gattCharacteristicByUrl.get(characteristicUrl);
							
							if(characteristic != null) {
								if(!_gattFieldsByCharacteristic.containsKey(characteristic)) {
									buildFieldsByCharacteristicMap(characteristic);
								} else {
									//_logger.warn("FieldsByCharacteristic Map already contains (Notification) " + characteristic.getName());
								}
								
								FieldHolder fieldHolder;
								String fieldName;
								String serviceUrl = characteristicUrl.getDeviceAddress() + "/" + characteristicUrl.getServiceUUID();
								String characUrl = serviceUrl + "/" + characteristicUrl.getCharacteristicUUID();
		
								for(Field field : _gattFieldsByCharacteristic.get(characteristic)) {
									fieldName = field.getName();
									fieldHolder = getParsedData(notificationData, fieldName, characteristic);
									_logger.info("Parsed data of characteristic " + characUrl);
									if(fieldHolder != null) {
										notifyRawDataReady(notificationData, fieldName, characUrl);
										notifyParsedDataReady(fieldHolder, fieldName, characUrl);
									}
								}
							} else {
								//_logger.warn("There is no characteristic with URL (Notification) " + characteristicUrl);
							}
						});
					}
				});
			}
		});
	}*/
	
	int inc;
	public void listenNewData() {
		String adapterUrl = getAdapterUrl();
		ArrayList<Band> bands = _db.getBands();
		inc = 0;
		for(Band band : bands) {
			String dataUrl = band.getDataUrl();
			String bandId = band.getBandId();
			_logger.info("Listening to Band <"+bandId+"> URL: "+band.getDataUrl());
			enableDeviceConnection(adapterUrl, dataUrl);

			URL characteristicUrl = new URL(adapterUrl + "/" + dataUrl.substring(0, dataUrl.lastIndexOf("/")));
			CharacteristicGovernor characteristicGovernor = _bleManager.getCharacteristicGovernor(characteristicUrl);
			_logger.info("Band <"+bandId+"> characteristic: "+characteristicUrl);
			
//			DeviceGovernor currentDevice = _bleManager.getDeviceGovernor(getDeviceAddressUrl(adapterUrl, dataUrl));
//			String deviceRssi = Short.toString(currentDevice.getRSSI());
//			
//			_logger.info("Band <"+bandId+"> characteristic: "+characteristicUrl + "<RSSI: "+deviceRssi+" db>");
			
			
			characteristicGovernor.whenReady(CharacteristicGovernor::isNotifiable).thenAccept(notifiable -> {
				if(notifiable) {
					_logger.info("Governor Ready ... ");
					characteristicGovernor.addValueListener(notificationData -> {
						_logger.info("Listening new value... ["+inc+"]");
						FieldHolder fieldHolder = getParsedData(notificationData, dataUrl, characteristicUrl, bandId);
						inc++;
						if(fieldHolder != null) {
							String parsedData = fieldHolder.toString();
							_db.insertDataDocument(band, parsedData);
						}
					});
				}
			});
		}
	}
	
	private String getAdapterUrl() {
		ArrayList<DiscoveredAdapter> adapters = new ArrayList<DiscoveredAdapter>(_bleManager.getDiscoveredAdapters());
		DiscoveredAdapter adapter = adapters.get(0);
		String protocolAndAdapterUrl = adapter.getURL().toString();
		String adapterUrl = protocolAndAdapterUrl.substring(protocolAndAdapterUrl.indexOf("/"));
		_logger.info("Adapter URL " + adapterUrl);
		
		return adapterUrl;
	}
	
	private void enableDeviceConnection(String adapterUrl, String dataUrl) {
//		String deviceAddress = getDeviceAddressFromDataUrl(dataUrl);
//		URL deviceUrl = new URL(adapterUrl + "/" + deviceAddress);
		
		URL deviceUrl = getDeviceAddressUrl(adapterUrl, dataUrl);
		_bleManager.getDeviceGovernor(deviceUrl, true);
		_logger.info("Connection enabled to device " + deviceUrl);
	}
	
	private void buildFieldsByCharacteristicMap(Characteristic characteristic) {
		Value value = characteristic.getValue();
		ArrayList<Field> fields = new ArrayList<Field>();
		fields.addAll(value.getFields());
		
		_gattFieldsByCharacteristic.put(characteristic, fields);
	}
	
	private FieldHolder getParsedData(byte[] rawData, String dataUrl, URL characteristicUrl, String bandId) {
		String fieldName = dataUrl.substring(dataUrl.lastIndexOf("/") + 1);
		String characteristicUuid = characteristicUrl.getCharacteristicUUID();
		GattResponse gattResponse = _gattParser.parse(characteristicUuid, rawData);
		FieldHolder fieldHolder = gattResponse.get(fieldName);
		_logger.info("Parsed Data: [" + bandId + ", " + fieldName + ", " + fieldHolder + "]");

		return fieldHolder;
	}
	
	private String getDeviceAddressFromDataUrl(String dataUrl) {
		return dataUrl.substring(0, dataUrl.indexOf("/"));
	}
	
	private URL getDeviceAddressUrl(String adapterUrl, String dataUrl) {
		return new URL(adapterUrl + "/" + getDeviceAddressFromDataUrl(dataUrl));
	}
	
	
	/**
	 * OLD VERSION
	 * @param rawData
	 * @param fieldName
	 * @param characteristic
	 * @return
	 *//*
	private FieldHolder getParsedData(byte[] rawData, String fieldName, Characteristic characteristic) {
		GattResponse gattResponse = _gattParser.parse(characteristic.getUuid(), rawData);
		FieldHolder fieldHolder = gattResponse.get(fieldName);
		
		_logger.info(" -----> PARSED DATA: [" + fieldName + ", " + fieldHolder + "]");

		return fieldHolder;
	}*/
	
	
/*	public void parseData(URL characteristicUrl) {
		CharacteristicGovernor characteristicGovernor = _bleManager.getCharacteristicGovernor(characteristicUrl);

		characteristicGovernor.whenReady(CharacteristicGovernor::isReadable).thenAccept(readable -> {
			if(readable) {
				characteristicGovernor.whenReady(CharacteristicGovernor::read).thenAccept(rawData -> {
					Characteristic characteristic = _gattCharacteristicByUrl.get(characteristicUrl);
					
					if(characteristic != null) {
						if(!_gattFieldsByCharacteristic.containsKey(characteristic)) {
							Value value = characteristic.getValue();
							ArrayList<Field> fields = new ArrayList<Field>();
							fields.addAll(value.getFields());
							
							_gattFieldsByCharacteristic.put(characteristic, fields);
							//_logger.warn("FieldCharacteristicMap has new characteristic: " + gattCharacteristic.getURL().getCharacteristicUUID());

							FieldHolder fieldHolder;
							GattResponse gattResponse = _gattParser.parse(characteristic.getUuid(), rawData);
	
							for(Field field : _gattFieldsByCharacteristic.get(characteristic)) {
								fieldHolder = gattResponse.get(field.getName());
								_logger.info(" -----> PARSED DATA: [" + field.getName() + ", " + fieldHolder + "]");
							}
						} else {
							  _logger.warn("FieldsByCharacteristic Map already contains " + characteristic.getName());
						}
					} else {
						_logger.warn("There is no characteristic with URL " + characteristicUrl);
					}
			    });
			} else {
				_logger.warn("The Characteristic with URL " + characteristicUrl + " is not readable");
			}
		});
	}*/
	
	public boolean isKnownCharacteristic(String characteristicUuid) {
		return _gattParser.isKnownCharacteristic(characteristicUuid);
	}
	
	public static DbOperations getDb() {
		return _db;
	}
}

package ble;

public class Band {

	private String _bandId;
	private String _dataUrl;
	
	public Band(String bandId, String dataUrl) {
		_bandId = bandId;
		_dataUrl = dataUrl;
	}
	
	public String getBandId() {
		return _bandId;
	}
	
	public String getDataUrl() {
		return _dataUrl;
	}
}

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.sputnikdev.bluetooth.URL;
import org.sputnikdev.bluetooth.gattparser.spec.Characteristic;
import org.sputnikdev.bluetooth.manager.DeviceGovernor;
import org.sputnikdev.bluetooth.manager.DiscoveredDevice;
import org.sputnikdev.bluetooth.manager.GattService;

import ble.BleManager;
import rest.RestServer;
import storage.DbOperations;
import utils.CallbackListener;

public class Main implements CallbackListener {
    
	final static Logger _logger = Logger.getLogger(Main.class);
	
    private static BleManager _bleManager;
    private static DbOperations _db;
    private static RestServer _server;
    
    public Main() {
    	_logger.info("Starting application ... ");
    	_bleManager = new BleManager(true, true, -1, -1, false);
    	_logger.info("Ble manager initialized ...");
    	//_bleManager.addCallbackListener(this);
    	_db = BleManager.getDb();
    	_logger.info("Ble DB Acquired...");
    	//_server = new RestServer(8080);
    	//new Thread(_server).start();
    	new RestServer(-1);
    }
    
	public static void main(String[] args) throws IllegalArgumentException {
		new Main();
		
		if(args.length > 0) {
			if(args[0].equals("export")) {
				_db.exportEachCollectionIndividuallyToJson();
			}
		}
		
		_logger.info("Starting...");
		
		
//		 Polar OH1
		String deviceUrl = "A0:9E:1A:2C:7C:A4";
		String serviceUuid = "0000180d-0000-1000-8000-00805f9b34fb";
		String characteristicUuid = "00002a37-0000-1000-8000-00805f9b34fb";
		String fieldName = "Heart Rate Measurement Value (uint8)";
		String dataUrl = deviceUrl + "/" + serviceUuid + "/" + characteristicUuid + "/" + fieldName;
		
		_db.insertBandDocument("HR01", dataUrl);
		
		// Xiaomi Mi 2
//		String deviceUrl = "EB:D8:42:9B:5C:D1";
//		String serviceUuid = "0000180d-0000-1000-8000-00805f9b34fb";
//		String characteristicUuid = "00002a37-0000-1000-8000-00805f9b34fb";
//		String fieldName = "Heart Rate Measurement Value (uint8)";
//		String dataUrl = deviceUrl + "/" + serviceUuid + "/" + characteristicUuid + "/" + fieldName;
		
//		_db.insertBandDocument("MI2HR", dataUrl);
		
		
		_bleManager.listenNewData();
		
//		_bleManager.startDiscovery();
//		_bleManager.discoverDevices();
	}
	
		
	
	@Override
	public void updateDiscoveredDevices(DiscoveredDevice discoveredDevice) {
		_db.insertDeviceDocument(discoveredDevice);
		_bleManager.discoverResolvedServicesByDevice(discoveredDevice);
	}
	
	@Override
	public void updateResolvedServices(String deviceUrl, GattService gattService) {
		if(_db.insertServiceDocument(gattService)) {
			_db.addServicesToDevice(deviceUrl);
		}
		
		//String serviceUuid = gattService.getURL().getServiceUUID();
		//if(serviceUuid.equals("0000180d-0000-1000-8000-00805f9b34fb")) {
			_bleManager.discoverCharacteristicsByServiceByDevice(gattService);
		//}
	}

	@Override
	public void updateCharacteristics(GattService gattService, URL characteristicUrl) {
		String serviceUrl = gattService.getURL().getDeviceAddress() + "/" + gattService.getURL().getServiceUUID();
		String characteristicUuid = characteristicUrl.getCharacteristicUUID();
		String characUrl = serviceUrl + "/" + characteristicUuid;
		
		if(_bleManager.isKnownCharacteristic(characteristicUuid)) {
			//_logger.info("Main was notified of new characteristic: " + characteristicUrl);
			Characteristic characteristic = _bleManager.getCharacteristicByUrl(characteristicUrl);
			
			if(_db.insertCharacteristicDocument(characUrl, characteristic)) {
				_db.addCharacteristicsToService(serviceUrl);
			}
			
			//_bleManager.parseData(characteristicUrl);
		} else {
			//_logger.warn("Unknown characteristic: " + characUrl);
			addUnknownCharacteristicToFile(characUrl);
		}
	}
	
	@Override
	public void updateRawData(byte[] rawData, String fieldName, String characteristicUrl) {
		if(_db.insertFieldDocument(rawData, fieldName, characteristicUrl)) {
			_db.addFieldsToCharacteristic(characteristicUrl);
		}
	}
	
	@Override
	public void updateParsedData(String parsedData, String fieldUrl) {
		//_db.insertDataDocument(parsedData, fieldUrl);
		_db.addDataToField(fieldUrl);
	}
	
	private void addUnknownCharacteristicToFile(String characteristicUrl) {
		String filepath = "/usr/project/ble-manager-jar/unknownCharacteristics.txt";
		
		try {
			//_logger.info("Writing unknown characteristic to file...");
			
			FileWriter fileWriter = new FileWriter(filepath, true);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			PrintWriter filePrinter = new PrintWriter(bufferedWriter);
			
			filePrinter.println(characteristicUrl);
			
			filePrinter.close();
			bufferedWriter.close();
			fileWriter.close();
		} catch (IOException e) {
			_logger.error("There is an IOException");
			e.printStackTrace();
		}
	}

	public void setDeviceListenerScheduler(DiscoveredDevice discoveredDevice) {
		ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
		scheduler.scheduleAtFixedRate(() -> {
			DeviceGovernor deviceGovernor = _bleManager.getDeviceGovernor(discoveredDevice.getURL());
			if(!deviceGovernor.isOnline() && !deviceGovernor.isConnected()) {
				_bleManager.removeInactiveDevice(discoveredDevice);
				scheduler.shutdown();
				
				try {
					if(!scheduler.awaitTermination(3, TimeUnit.SECONDS)) {
						scheduler.shutdownNow();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}, 30, 60, TimeUnit.SECONDS);
	}
}
